all: libbruh.so

libbruh.so: bruh.c
        gcc -Wall -fPIC -shared -o libbruh.so bruh.c -ldl

.PHONY clean:
        rm -f libbruh.so
